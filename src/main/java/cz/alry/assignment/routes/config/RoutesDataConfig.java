package cz.alry.assignment.routes.config;

import java.util.List;
import cz.alry.assignment.routes.client.data.GithubDataClient;
import cz.alry.assignment.routes.data.CountriesStructure;
import cz.alry.assignment.routes.data.CountriesStructureImpl;
import cz.alry.assignment.routes.data.Country;
import feign.Feign;
import feign.jackson.JacksonDecoder;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Creates data for the {@link cz.alry.assignment.routes.service.RoutesService}
 */
@Configuration
@Slf4j
public class RoutesDataConfig {

    @Bean
    public CountriesStructure countriesStructure() {
        final var countries = getCountries();
        return new CountriesStructureImpl(countries);
    }

    private List<Country> getCountries() {
        GithubDataClient githubDataClient = getClient();
        final List<Country> countriesData = githubDataClient.getCountriesData();
        log.info("Obtained {} data items from github.", countriesData.size());
        return countriesData;
    }

    private GithubDataClient getClient() {
        GithubDataClient githubDataClient = Feign.builder()
                .decoder(new JacksonDecoder())
                .target(GithubDataClient.class, GithubDataClient.GITHUB_RAW_HOST);

        return githubDataClient;
    }
}
