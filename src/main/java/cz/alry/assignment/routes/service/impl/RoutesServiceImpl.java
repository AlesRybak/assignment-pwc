package cz.alry.assignment.routes.service.impl;

import java.util.List;
import java.util.Optional;
import cz.alry.assignment.routes.data.CountriesStructure;
import cz.alry.assignment.routes.data.CountriesStructureImpl;
import cz.alry.assignment.routes.data.Country;
import cz.alry.assignment.routes.exception.CountryNotFoundException;
import cz.alry.assignment.routes.service.RoutesService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * Default implementation of the {@link RoutesService}.
 */
@Service
@Slf4j
public class RoutesServiceImpl implements RoutesService {

    private final CountriesStructure countries;

    public RoutesServiceImpl(CountriesStructure countries) {
        this.countries = countries;
    }

    @Override
    public Optional<List<String>> searchRoute(String start, String target) {
        if (!countries.existsByCode(start)) {
            throw new CountryNotFoundException(start);
        }
        if (!countries.existsByCode(target)) {
            throw new CountryNotFoundException(target);
        }

        var route = countries.findShortestRoute(start, target);
        if (route != null) {
            try {
                Thread.sleep(5 * route.size());
            } catch (java.lang.InterruptedException e) {
            }
        }
        return Optional.of(route);
    }

}
