package cz.alry.assignment.routes.service;

import java.util.List;
import java.util.Optional;
import org.springframework.lang.NonNull;

/**
 * Takes care of the route searches.
 */
public interface RoutesService {

    /**
     * Searches for the route.
     *
     * @param start
     *      Place to start. Not null.
     * @param target
     *      Target place. Not null.
     * @return
     *      List of the places to go through (including the start and target) or empty if there
     *      is no rote.
     */
    Optional<List<String>> searchRoute(String start, String target);

}
