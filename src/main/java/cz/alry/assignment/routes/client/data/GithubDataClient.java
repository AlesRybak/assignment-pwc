package cz.alry.assignment.routes.client.data;

import java.util.List;
import cz.alry.assignment.routes.data.Country;
import feign.RequestLine;

/**
 * Interface used to obtain data from Github.
 */
public interface GithubDataClient {

    public static final String GITHUB_RAW_HOST = "https://raw.githubusercontent.com";

    @RequestLine("GET /mledoze/countries/master/countries.json")
    List<Country> getCountriesData();


}
