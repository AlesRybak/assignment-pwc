package cz.alry.assignment.routes.exception;

/**
 * Exception that is used in places where implementation is missing
 */
public class TodoException extends RuntimeException {

    public TodoException(String message) {
        super(message);
    }

}
