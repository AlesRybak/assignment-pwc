package cz.alry.assignment.routes.data;

import java.util.List;
import java.util.stream.Stream;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.MethodSource;
import static cz.alry.assignment.routes.data.CountryMother.AUSTRIA;
import static cz.alry.assignment.routes.data.CountryMother.CZECHIA;
import static cz.alry.assignment.routes.data.CountryMother.HUNGARY;
import static cz.alry.assignment.routes.data.CountryMother.ITALY;
import static cz.alry.assignment.routes.data.CountryMother.MEXICO;
import static cz.alry.assignment.routes.data.CountryMother.POLAND;
import static cz.alry.assignment.routes.data.CountryMother.SLOVAKIA;
import static cz.alry.assignment.routes.data.CountryMother.czechOnlyCountryList;
import static cz.alry.assignment.routes.data.CountryMother.emptyCountryList;
import static cz.alry.assignment.routes.data.CountryMother.moreCountryList;
import static cz.alry.assignment.routes.data.CountryMother.vysegradCountryList;
import static org.assertj.core.api.Assertions.assertThat;

/**
 * Unit tests for the {@link CountriesStructureImpl}
 */
class CountriesStructureImplTest {

    @Test
    void existsByCode_returnFalseForEmpty() {
        // given
        var countries = new CountriesStructureImpl(emptyCountryList());
        var wrongCountryCode = "WRG";

        // when
        final var actual = countries.existsByCode(wrongCountryCode);

        // then
        assertThat(actual).isFalse();
    }

    @ParameterizedTest
    @CsvSource(value = {
            "WRG, false",
            "CZE, true"})
    void existsByCode(String code, boolean expected) {
        // given
        var countries = new CountriesStructureImpl(czechOnlyCountryList());

        // when
        final var actual = countries.existsByCode(code);

        // then
        assertThat(actual).isEqualTo(expected);
    }

    @ParameterizedTest
    @CsvSource(value = {
            "WRG, CZE",
            "CZE, WRG"})
    void findShortestRoute_returnEmptyIfCountryNotFound(String start, String target) {
        // given
        var countries = new CountriesStructureImpl(czechOnlyCountryList());

        // when
        final var actual = countries.findShortestRoute(start, target);

        // then
        assertThat(actual).isEmpty();
    }

    @Test
    void findShortestRoute_returnSameIfSourceAndTargetEquals() {
        // given
        var countries = new CountriesStructureImpl(czechOnlyCountryList());

        // when
        final var actual = countries.findShortestRoute(CZECHIA, CZECHIA);

        // then
        assertThat(actual).containsOnly(CZECHIA);
    }

    static Stream<Arguments> findShortestRoute_findsSingleShortestRouteInVysegrad() {
        return Stream.of(
                Arguments.of(POLAND, HUNGARY, List.of(POLAND, SLOVAKIA, HUNGARY)),
                Arguments.of(CZECHIA, HUNGARY, List.of(CZECHIA, SLOVAKIA, HUNGARY)),
                Arguments.of(HUNGARY, POLAND, List.of(HUNGARY, SLOVAKIA, POLAND)),
                Arguments.of(SLOVAKIA, CZECHIA, List.of(SLOVAKIA, CZECHIA)),
                Arguments.of(CZECHIA, SLOVAKIA, List.of(CZECHIA, SLOVAKIA)));
    }

    @ParameterizedTest
    @MethodSource
    void findShortestRoute_findsSingleShortestRouteInVysegrad(String start, String target, List<String> expected) {
        // given
        var countries = new CountriesStructureImpl(vysegradCountryList());

        // when
        final var actual = countries.findShortestRoute(start, target);

        // then
        assertThat(actual).isEqualTo(expected);
    }

    static Stream<Arguments> findShortestRoute_findsShortestRoute() {
        return Stream.of(
                Arguments.of(POLAND, ITALY, List.of(
                        List.of(POLAND, CZECHIA, AUSTRIA, ITALY),
                        List.of(POLAND, SLOVAKIA, AUSTRIA, ITALY))),
                Arguments.of(ITALY, POLAND, List.of(
                        List.of(ITALY, AUSTRIA, CZECHIA, POLAND),
                        List.of(ITALY, AUSTRIA, SLOVAKIA, POLAND))),
                Arguments.of(CZECHIA, ITALY, List.of(List.of(CZECHIA, AUSTRIA, ITALY))),
                Arguments.of(ITALY, CZECHIA, List.of(List.of(ITALY, AUSTRIA, CZECHIA))),
                Arguments.of(CZECHIA, SLOVAKIA, List.of(List.of(CZECHIA, SLOVAKIA))));
    }

    @ParameterizedTest
    @MethodSource
    void findShortestRoute_findsShortestRoute(String start, String target, List<List<String>> expected) {
        // given
        var countries = new CountriesStructureImpl(moreCountryList());

        // when
        final var actual = countries.findShortestRoute(start, target);

        // then
        assertThat(actual).isIn(expected);
    }

    @Test
    void findShortestRoute_noRouteExistsReturnsEmptyList() {
        // given
        var countries = new CountriesStructureImpl(moreCountryList());

        // when
        final var actual = countries.findShortestRoute(CZECHIA, MEXICO);

        // then
        assertThat(actual).isEmpty();
    }
}