/*
 * Copyright (C) Lundegaard a.s. 2021 - All Rights Reserved
 *
 * Proprietary and confidential. Unauthorized copying of this file, via any
 * medium is strictly prohibited.
 */
package cz.alry.assignment.routes.resource;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.ActiveProfiles;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles(profiles = {"test", "IT"})
@Slf4j
public abstract class AbstractResourceIT {

    public static final String TEST_HOST = "localhost";

    @LocalServerPort
    int randomServerPort;

    protected String getServerUrl() {
        return "http://" + TEST_HOST + ":" + randomServerPort;
    }

    protected String getServerUrlWithApiVersion() {
        return getServerUrl();
    }
}
