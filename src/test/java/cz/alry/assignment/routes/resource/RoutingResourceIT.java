/*
 * Copyright (C) Lundegaard a.s. 2021 - All Rights Reserved
 *
 * Proprietary and confidential. Unauthorized copying of this file, via any
 * medium is strictly prohibited.
 */
package cz.alry.assignment.routes.resource;

import cz.alry.assignment.routes.service.RoutesService;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import static org.assertj.core.api.Assertions.assertThat;

public class RoutingResourceIT extends AbstractResourceIT {

    private static final String RESOURCE_PATH_ROUTING = "/routing";

    @Test
    void routingCzeSvkWorks() {
        // when - Send OTP sms
        Response response = RestAssured.given()
                .get(getRoutingResourceUrl("/{start}/{target}"), "CZE", "SVK");

        // then - Sms sent
        assertThat(response.statusCode()).isEqualTo(HttpStatus.OK.value());
        assertThat(response.body().print()).isEqualTo("[\"CZE\",\"SVK\"]");
    }

    private String getRoutingResourceUrl(String path) {
        return getServerUrlWithApiVersion() + RESOURCE_PATH_ROUTING + path;
    }
}
