package cz.alry.assignment.routes.service.impl;

import cz.alry.assignment.routes.data.CountriesStructure;
import cz.alry.assignment.routes.data.CountriesStructureImpl;
import cz.alry.assignment.routes.exception.CountryNotFoundException;
import cz.alry.assignment.routes.service.RoutesService;
import org.junit.jupiter.api.Test;
import static cz.alry.assignment.routes.data.CountryMother.CZECHIA;
import static cz.alry.assignment.routes.data.CountryMother.czechOnlyCountryList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;

/**
 * Unit tests for the {@link RoutesServiceImpl}.
 */
class RoutesServiceImplTest {

    @Test
    void wrongStartCountryThrows() {
        // given
        CountriesStructure countries = new CountriesStructureImpl(czechOnlyCountryList());
        RoutesService routesService = new RoutesServiceImpl(countries);
        var wrongCountryCode = "WRG";

        // when
        Throwable thrown = catchThrowable(() -> {
            routesService.searchRoute(wrongCountryCode, CZECHIA);
        });

        // then
        assertThat(thrown)
                .isInstanceOf(CountryNotFoundException.class)
                .hasMessageContaining(wrongCountryCode);
    }

    @Test
    void wrongTargetCountryThrows() {
        // given
        CountriesStructure countries = new CountriesStructureImpl(czechOnlyCountryList());
        RoutesService routesService = new RoutesServiceImpl(countries);
        var wrongCountryCode = "WRG";

        // when
        Throwable thrown = catchThrowable(() -> {
            routesService.searchRoute(CZECHIA, wrongCountryCode);
        });

        // then
        assertThat(thrown)
                .isInstanceOf(CountryNotFoundException.class)
                .hasMessageContaining(wrongCountryCode);
    }

}