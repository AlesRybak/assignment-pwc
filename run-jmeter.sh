#!/bin/sh

if [ $# -ne 4 ]; then
    echo "Usage: $0 <jmx-file> <threads> <duration> <ramp>"
    exit 1
fi

set -xe

JMX_FILE="$1"
PARAM_THREADS="$2"
PARAM_DURATION="$3"
PARAM_RAMP="$4"

TEST_NAME=$(basename $JMX_FILE | sed s/.jmx//)
TIMESTAMP=$(date "+%F_%H-%M-%I")
TARGET_DIR="target/${TEST_NAME}_${PARAM_THREADS}_${PARAM_DURATION}_${PARAM_RAMP}_${TIMESTAMP}"

mkdir -p $TARGET_DIR $TARGET_DIR/out
cp jmeter/user.properties $TARGET_DIR/user.properties
cp $JMX_FILE $TARGET_DIR/test.jmx


cd $TARGET_DIR
jmeter -n -t test.jmx -p user.properties -Jthreads=$PARAM_THREADS -Jduration=$PARAM_DURATION -Jramp=$PARAM_RAMP -l test.jtl -j test.log
jmeter -g test.jtl -o out

echo "Result is here:" >&2
echo "  $TARGET_DIR/out/index.html" >&2

open out/index.html
