# Assignment - routes

## Assignment

Your task is to create a simple Spring Boot service, that is able to calculate any possible land route from one country to another. The objective is to take a list of country data in JSON format and calculate the route by utilizing individual countries border information. 
 
* Spring Boot, Maven
* Data link: https://raw.githubusercontent.com/mledoze/countries/master/countries.json
* The application exposes REST endpoint /routing/{origin}/{destination} that returns a list of border crossings to get from origin to destination
* Single route is returned if the journey is possible
* Algorithm needs to be efficient
* If there is no land crossing, the endpoint returns HTTP 400
* Countries are identified by cca3 field in country data
* HTTP request sample (land route from Czech Republic to Italy):

```
GET /routing/CZE/ITA HTTP/1.0
{
   "route": ["CZE", "AUT", "ITA"] 
}
```


## Usage

### Docker

Safer way to build and run the project is using Docker, since you don't need to have precise Java and Maven on your system.

To build the image, run from root folder following command:

```bash
docker build -t alry.cz/routes:latest .
```

Now the image should be built. To run it locally just use:

```bash
docker run -d -p 8080:8080 --name routes alry.cz/routes:latest
```

After that is running on port `8080` (make sure port is free beforehand). To test it use curl (or just browser):

```bash
curl http://localhost:8080/routing/CZE/ITA
```

After testing enough use following to remove the container:

```bash
docker rm -f routes
```

### Maven

If you don't have Docker, but you have Maven 3.6+ and Java 17 installed locally, you could build and run the project using Maven.

To build and run just:

```bash
mvn clean install spring-boot:run
```

After that is running on port `8080` (make sure port is free beforehand). To test it use curl (or just browser):

```bash
curl http://localhost:8080/routing/CZE/ITA
```

After testing enough use `ctrl+C` to finish the process. 


## Solution description

* I've used the JGraphT library (https://jgrapht.org) to handle the graph stuff heavy lifting
* The `RoutesServiceImpl` is more or less only facade for the `CountriesStructure`, which gives us quite nice way to implement other graphing methods.
* The tests would require more work, but for this simple example I've skipped some of the edge cases and some explicit validataions. In other words, API could be more user-friendly.
* I haven't tackled situation, when there are multiple shortest routes. I just return first and best, this is also behavior of the library.
* The data for the application is downloaded from the GitHub. This makes it dependant on it while building (it is needed to run tests) and running. The data are downloaded only once (when starting the app).
