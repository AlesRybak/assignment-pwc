##
## Stage 1 - Build using Maven
##

FROM maven:3.8.3-eclipse-temurin-17 as BUILD

WORKDIR /project

COPY pom.xml /project/
RUN mvn org.apache.maven.plugins:maven-dependency-plugin:3.2.0:go-offline

COPY . /project/
RUN mvn clean install


##
## Stage 2 - Create runnable image
##

FROM eclipse-temurin:17-jdk

ENV APP_HOME=/opt/routes

COPY --from=BUILD /project/target/assignment-routes-0.0.1-SNAPSHOT.jar "$APP_HOME/routes.jar"

WORKDIR /opt/routes

CMD ["java", \
    "-jar", \
    "routes.jar"]

EXPOSE 8080
